module Heroq
  class Repo
    def initialize(env)
      @env = env

      FileUtils.cd(env.coverage_path)
      ::Grit.debug = true
      @grit = ::Grit::Repo.init('.')
    end

    def self.init(env)
      repo = self.new(env)
      repo.remote_add
      repo.add
      repo.commit

      yield repo
    end

    def remote_add
      @grit.remote_add(@env.remote_name, @env.remote_url)
    end

    def add
      @grit.add('.')
    end

    def commit
      @grit.commit_all(@env.revision)
    end

    def push
      Grit::Git.with_timeout(1000) { @grit.git.push({}, @env.remote_name, '-f') }
    end
  end
end
