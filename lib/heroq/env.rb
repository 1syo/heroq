module Heroq
  class Env
    def home
      ENV['HOME']
    end

    def pwd
      ENV['PWD']
    end

    def travis_build_number
      ENV['TRAVIS_BUILD_NUMBER']
    end

    def travis_branch
      ENV['TRAVIS_BRANCH']
    end

    def heroku_api_key
      ENV['HEROKU_API_KEY']
    end

    def deploy_to
      ENV['DEPLOY_TO']
    end

    def revision
      "heroq-#{travis_build_number}"
    end

    def ssh_dir
      File.join(home, '.ssh')
    end

    def backup_dir
      File.join(home, ".ssh_#{revision}")
    end

    def publick_key
      File.join(home, '.ssh', "#{revision}.pub")
    end

    def private_key
      File.join(home, '.ssh', revision)
    end

    def config_path
      File.join(home, '.ssh', 'config')
    end

    def remote_host
      "#{revision}.heroku.com"
    end

    def coverage_path
      File.join(pwd, 'coverage')
    end

    def remote_url
      "git@#{remote_host}:#{deploy_to}"
    end

    def remote_name
      'coverage'
    end

    def execute?
      File.exists?(coverage_path) && ENV.has_key?('HEROKU_API_KEY') && (ENV['HEROKU_API_KEY'].length > 0)
    end
  end
end
