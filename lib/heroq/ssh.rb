module Heroq
  class Ssh
    def initialize(env)
      @env = env
      @heroku = ::Heroku::API.new(api_key: @env.heroku_api_key)
    end

    def self.start(env)
      ssh = self.new(env)
      ssh.backup_ssh_dir
      ssh.generate_config
      ssh.generate_sshkey
      ssh.push_sshkey

      yield ssh

      ssh.restore_ssh_dir
      ssh.remove_sshkey
    end

    def backup_ssh_dir
      FileUtils.mv(@env.ssh_dir, @env.backup_dir)
      FileUtils.mkdir(@env.ssh_dir)
    end

    def restore_ssh_dir
      FileUtils.rm_rf(@env.ssh_dir)
      FileUtils.mv(@env.backup_dir, @env.ssh_dir)
    end

    def generate_sshkey
      `yes | ssh-keygen -q -f #{@env.private_key} -t rsa -N "" -C #{@env.revision} -V "+1h"`
    end

    def generate_config
      open(@env.config_path, "w") {|fd| fd.write(config)}
    end

    def push_sshkey
      open(@env.publick_key, "r") {|fd| @heroku.post_key(fd.read) }
    end

    def remove_sshkey
      @heroku.delete_key(@env.revision)
    end

    def config
        <<-__CONFIG__
Host #{@env.remote_host}
  HostName heroku.com
  IdentityFile #{@env.private_key}
  TCPKeepAlive yes
  IdentitiesOnly yes
  StrictHostKeyChecking no
  CheckHostIP no
  UserKnownHostsFile=/dev/null
__CONFIG__
    end
  end
end
