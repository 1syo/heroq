require 'heroq/version'
require 'heroq/env'
require 'heroq/repo'
require 'heroq/ssh'
require 'fileutils'
require 'heroku-api'
require 'grit'

module Heroq
  extend self

  def run
    env = Heroq::Env.new
    return unless env.execute?

    Heroq::Ssh.start(env) do |ssh|
      Heroq::Repo.init(env) { |repo| repo.push }
    end
  end

  module_function :run
end
