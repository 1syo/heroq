# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'heroq/version'

Gem::Specification.new do |gem|
  gem.name          = "heroq"
  gem.version       = Heroq::VERSION
  gem.authors       = ["TAKAHASHI Kazunari"]
  gem.email         = ["takahashi@1syo.net"]
  gem.description   = %q{The Heroq is a program to push to heroku results simplecov.}
  gem.summary   = %q{The Heroq is a program to push to heroku results simplecov.}
  gem.homepage      = "https://github.com/1syo/heroq"

  gem.executables   = ["heroq"]
  gem.require_paths = ["lib"]
  gem.files         = `git ls-files`.split($/)
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})

  gem.licenses = ['MIT']

  gem.add_dependency 'grit'
  gem.add_dependency 'heroku-api'

  gem.add_development_dependency 'bundler'
  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'rdoc'
  gem.add_development_dependency 'rspec', ['>= 2.12']
  gem.add_development_dependency 'pry'
  gem.add_development_dependency 'forgery'
end
