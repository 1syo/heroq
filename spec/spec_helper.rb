$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'bundler/setup'
Bundler.require
require 'pry'
require 'forgery'
require 'fileutils'

RSpec.configure do |config|
  config.mock_with :rspec
end
