require 'spec_helper'

describe Heroq::Repo do
  let(:revision) { "heroq" }
  let(:coverage_path) { "tmp/coverage" }
  let(:env) { Heroq::Env.new }
  let(:deploy_to) { Forgery::Basic.text }

  subject { Heroq::Repo.new(env) } 

  before do
    Heroq::Env.any_instance.stub(:revision) { revision }
    Heroq::Env.any_instance.stub(:coverage_path) { coverage_path }
    Heroq::Env.any_instance.stub(:deploy_to) { deploy_to }
    FileUtils.mkdir_p(env.coverage_path)
  end

  describe '#remote_add' do
    before do
      Grit::Repo.any_instance.should_receive(:remote_add) { true }
    end
    it { expect{ subject.remote_add }.to_not raise_error }
  end

  describe '#add' do
    before do
      FileUtils.touch(File.join(coverage_path, 'index.html'))
      Grit::Repo.any_instance.should_receive(:add) { true }
    end

    it { expect{ subject.add }.to_not raise_error }
  end

  describe '#commit' do
    before do
      FileUtils.touch(File.join(coverage_path, 'index.html')) 
      subject.add

      Grit::Repo.any_instance.should_receive(:commit_all) { true }
    end
    it { expect{ subject.commit }.to_not raise_error }
  end

  describe '#push' do
    before do
      FileUtils.touch(File.join(coverage_path, 'index.html')) 
      subject.add
      subject.commit

      Grit::Git.any_instance.should_receive(:native) { true }
    end

    it { expect{ subject.push }.to_not raise_error }
  end

  after do
    FileUtils.remove_dir(env.coverage_path, true)
  end
end
