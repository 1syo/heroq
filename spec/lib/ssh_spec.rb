require 'spec_helper'

describe Heroq::Ssh do
  subject { Heroq::Ssh.new(Heroq::Env.new) }

  describe "#generate_sshkey" do
    let(:revision) { "heroq" }
    let(:private_key) { "tmp/.ssh/#{revision}" }

    before do
      FileUtils.mkdir_p("tmp/.ssh")
      Heroq::Env.any_instance.stub(:private_key) { private_key }
      Heroq::Env.any_instance.stub(:revision) { revision }
      subject.generate_sshkey
    end

    it "private key exists" do
      File.exists?(private_key).should be_true
    end

    it "public key exists" do
      File.exists?(private_key + ".pub").should be_true
    end

    it "public key include revision" do
      open(private_key + ".pub", "r") do |fd|
        fd.read.should include revision
      end
    end

    after do
      FileUtils.rm_rf("tmp")
    end
  end

  describe "#generate_config" do
    let(:revision) { "heroq" }
    let(:private_key) { "tmp/.ssh/#{revision}" }
    let(:config_path) { "tmp/.ssh/config" }

    before do
      FileUtils.mkdir_p("tmp/.ssh")
      Heroq::Env.any_instance.stub(:private_key) { private_key }
      Heroq::Env.any_instance.stub(:revision) { revision }
      Heroq::Env.any_instance.stub(:config_path) { config_path }
      subject.generate_config
    end

    it "config file exists" do
      File.exists?(config_path).should be_true
    end

    it "config file include revision" do
      open(config_path, "r") do |fd|
        fd.read.should include "Host #{revision}.heroku.com"
      end
    end

    it "config file include private key" do
      open(config_path, "r") do |fd|
        fd.read.should include "IdentityFile #{private_key}"
      end
    end

    after do
      FileUtils.rm_rf("tmp")
    end
  end

  describe "#backup_ssh_dir" do
    before do
      FileUtils.should_receive(:mv) { true }
      FileUtils.should_receive(:mkdir) { true }
    end

    its(:backup_ssh_dir) { should be_true }
  end

  describe "#restore_ssh_dir" do
    before do
      FileUtils.should_receive(:mv) { true }
      FileUtils.should_receive(:rm_rf) { true }
    end

    its(:restore_ssh_dir) { should be_true }
  end

  describe "#remove_sshkey" do
    before do
      Heroku::API.any_instance.should_receive(:delete_key) { true }
    end

    its(:remove_sshkey) { should be_true }
  end


  describe '.start' do
    let(:revision) { "heroq" }
    let(:private_key) { "tmp/.ssh/#{revision}" }
    let(:public_key) { "tmp/.ssh/#{revision}.pub" }
    let(:config_path) { "tmp/.ssh/config" }

    before do
      Heroq::Ssh.any_instance.should_receive(:push_sshkey) { true }
      Heroq::Ssh.any_instance.stub(:push_sshkey) { true }
      Heroq::Ssh.any_instance.stub(:restore_ssh_dir) { true }
      Heroq::Ssh.any_instance.stub(:remove_sshkey) { true }
      Heroq::Ssh.any_instance.stub(:backup_ssh_dir) { true }
      Heroq::Env.any_instance.stub(:private_key) { private_key }
      Heroq::Env.any_instance.stub(:public_key) { public_key }
      Heroq::Env.any_instance.stub(:revision) { revision }
      Heroq::Env.any_instance.stub(:config_path) { config_path }
      FileUtils.stub(:mv) { true }
      FileUtils.stub(:mkdir) { true }
      FileUtils.stub(:rm_rf) { true }
      FileUtils.mkdir_p("tmp/.ssh")
    end

    it do
      Heroq::Ssh.start(Heroq::Env.new) do |ssh|
        ssh.should be_kind_of Heroq::Ssh
      end
    end

    after do
      FileUtils.rm_rf("tmp")
    end
  end
end
