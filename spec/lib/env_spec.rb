require 'spec_helper'

describe Heroq::Env do
  let(:home) { "tmp" }
  let(:pwd) { "tmp/build" }
  let(:travis_build_number) { Forgery::Basic.number }
  let(:deploy_to) { Forgery::Basic.text }

  before do
    Heroq::Env.any_instance.stub(:home) { home }
    Heroq::Env.any_instance.stub(:pwd) { pwd }
    Heroq::Env.any_instance.stub(:travis_build_number) { travis_build_number }
    Heroq::Env.any_instance.stub(:deploy_to) { deploy_to }
  end

  describe "revision" do
    its(:revision) { should eq "heroq-#{travis_build_number}" }
  end

  describe "#publick_key" do
    its(:publick_key) { should eq home + "/.ssh/heroq-#{travis_build_number}.pub" }
  end

  describe "#private_key" do
    its(:private_key) { should eq home + "/.ssh/heroq-#{travis_build_number}" }
  end

  describe "#config_path" do
    its(:config_path) { should eq home + "/.ssh/config" }
  end

  describe "#remote_host" do
    its(:remote_host) { should eq "heroq-#{travis_build_number}.heroku.com" }
  end

  describe "#coverage_path" do
    its(:coverage_path) { should eq pwd + "/coverage" }
  end

  describe "#remote_url" do
    its(:remote_url) { should eq  "git@heroq-#{travis_build_number}.heroku.com:#{deploy_to}"}
  end

  describe ".execute?" do
    context "all true" do
      before do
        File.stub(:exists?) { true }
        ENV.stub(:has_key?) { true }
        ENV.stub_chain(:[], :length) { 1 }
      end

      its(:execute?) { should be_true }
    end

    context "not exists config path" do
      before do
        File.stub(:exists?) { false }
        ENV.stub(:has_key?) { true }
        ENV.stub_chain(:[], :length) { 1 }
      end

      its(:execute?) { should be_false }
    end

    context "not have HEROKU_API_KEY" do
      before do
        File.stub(:exists?) { true }
        ENV.stub(:has_key?) { false }
        ENV.stub_chain(:[], :length) { 1 }
      end

      its(:execute?) { should be_false }
    end

    context "HEROKU_API_KEY length equal 0" do
      before do
        File.stub(:exists?) { true }
        ENV.stub(:has_key?) { true }
        ENV.stub_chain(:[], :length) { 0 }
      end

      its(:execute?) { should be_false }
    end
  end
end
